function Vector() {
    switch (arguments.length) {
        case 1:
            this.x = arguments[0].x;
            this.y = arguments[0].y;
            this.z = arguments[0].z;
            break;
        case 3:
            this.x = arguments[0];
            this.y = arguments[1];
            this.z = arguments[2];
            break;
    }
    this.sq_magnitude = this.x * this.x + this.y * this.y + this.z * this.z;
    this.magnitude = Math.sqrt(this.sq_magnitude);
};

Vector.prototype.dot = function(v) {
    return (this.x * v.x + this.y * v.y + this.z * v.z);
};

Vector.prototype.cross = function(v) {
    return new Vector(this.y * v.z - this.z * v.y,
                      this.z * v.x - this.x * v.z,
                      this.x * v.y - this.y * v.x);
};

Vector.prototype.normalize = function() {
    var inv_norm = 1 / this.magnitude;
    return new Vector(this.x * inv_norm, this.y * inv_norm, this.z * inv_norm);
};

Vector.prototype.add = function(v) {
    return (isNaN(v)
            ? new Vector(this.x + v.x, this.y + v.y, this.z + v.z)
            : new Vector(this.x + v,   this.y + v,   this.z + v));
};

Vector.prototype.sub = function(v) {
    return (isNaN(v)
            ? new Vector(this.x - v.x, this.y - v.y, this.z - v.z)
            : new Vector(this.x - v,   this.y - v,   this.z - v));
};

Vector.prototype.invert = function() {
    return new Vector(-this.x, -this.y, -this.z);
};

Vector.prototype.mult = function(n) {
    return (isNaN(n)
            ? new Vector(this.x * n.x, this.y * n.y, this.z * n.z)
            : new Vector(this.x * n, this.y * n, this.z * n));
};

Vector.prototype.div = function(n) {
    return new Vector(this.x / n, this.y / n, this.z / n);
};

Vector.prototype.reflect = function(v) {
    var inverted_self = this.invert();
    var reflected_dir = v.mult(2 * inverted_self.dot(v)).sub(inverted_self);
    return reflected_dir;
};

Vector.INF = new Vector(-Infinity, -Infinity, -Infinity);
