/**
 * Wrapper class for the HTML5 <canvas> element.
 * Provides abstractions for pixel value setting/getting and screen updating
 */
function Canvas(canvas_id, width, height) {
    var canvas = document.getElementById(canvas_id);
    canvas.width = width;
    canvas.height = height;
    canvas.style.cssText = 'width:' + width + 'px; height:' + height + 'px;';
    var render_context = canvas.getContext('2d');
    var image_data = render_context.getImageData(0, 0, width, height);

    this._render_context = render_context;
    this._image_data = image_data;
    this.width = width;
    this.height = height;
    this.aspect_ratio = width / height;
};

/**
 * Set pixel (x, y) to color.
 * Color should be an object with r/g/b attributes where r,g,b are in [0..1]
 */
Canvas.prototype.set_pixel = function(x, y, color) {
    var idx = this.xy_to_idx(x, y);
    var red = 255 * Math.max(0, Math.min(1, color.r));
    var green = 255 * Math.max(0, Math.min(1, color.g));
    var blue = 255 * Math.max(0, Math.min(1, color.b));
    var alpha = 255 * Math.max(0, Math.min(1, color.a));
    this._image_data.data[idx + 0] = red;
    this._image_data.data[idx + 1] = green;
    this._image_data.data[idx + 2] = blue;
    this._image_data.data[idx + 3] = alpha;
};

/**
 * Returns the color of the pixel at (x, y) as an rgb-triplet.
 */
Canvas.prototype.get_pixel = function(x, y) {
    var idx = this.xy_to_idx(x, y);
    var r = this._image_data.data[idx + 0] / 255;
    var g = this._image_data.data[idx + 1] / 255;
    var b = this._image_data.data[idx + 2] / 255;
    var a = this._image_data.data[idx + 3] / 255;
    return new Color(r, g, b, a);
};

Canvas.prototype.xy_to_idx = function(x, y) {
    return 4 * (x + y * this.width);
};

/**
 * Updates the canvas by drawing contents of the pixel buffer to the screen.
 */
Canvas.prototype.draw = function() {
    this._render_context.putImageData(this._image_data, 0, 0);
};
