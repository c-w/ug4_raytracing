function Matrix(m) {
    this.row0 = new Vector(m[0][0], m[0][1], m[0][2]);
    this.row1 = new Vector(m[1][0], m[1][1], m[1][2]);
    this.row2 = new Vector(m[2][0], m[2][1], m[2][2]);
};

Matrix.prototype.mult = function(v) {
    return new Vector(this.row0.dot(v), this.row1.dot(v), this.row2.dot(v));
};
