function Color() {
    switch (arguments.length) {
        case 1:
            this.r = arguments[0].r;
            this.g = arguments[0].g;
            this.b = arguments[0].b;
            this.a = arguments[0].a || 1;
            break;
        case 3:
            this.r = arguments[0];
            this.g = arguments[1];
            this.b = arguments[2];
            this.a = 1;
            break;
        case 4:
            this.r = arguments[0];
            this.g = arguments[1];
            this.b = arguments[2];
            this.a = arguments[3];
            break;
    }
};

Color.prototype.mult = function(n) {
    return new Color(this.r * n, this.g * n, this.b * n, this.a);
};

Color.prototype.add = function(c) {
    return new Color(this.r + c.r, this.g + c.g, this.b + c.b, this.a);
};

Color.prototype.clip = function(t) {
    return new Color(Math.min(t, this.r),
                     Math.min(t, this.g),
                     Math.min(t, this.b),
                     Math.min(t, this.a));
};

Color.prototype.intensity = function() {
    return 0.2989 * this.r + 0.5870 * this.g + 0.1140 * this.b;
};

Color.prototype.luminance = function() {
    return 0.2126 * this.r + 0.715160 * this.g + 0.072169 * this.b;
};

Color.prototype.correct_exposure = function(exposure) {
    return new Color(1 - Math.exp(color.r * exposure),
                     1 - Math.exp(color.g * exposure),
                     1 - Math.exp(color.b * exposure));
};

Color.prototype.invert = function() {
    return new Color(1 - this.r, 1 - this.g, 1 - this.b);
};
