var ShapesDemo = {
    GlobalAmbient: 0.15,
    BackgroundColor: {r: 0, g: 0, b: 0},

    Camera: {
        position:  {x: 20, y: 10, z: -50},
        direction: {x: 0, y: 0, z: 1},
        fov:       168,
    },

    Lights: [
        {
            position: {x: 5, y: 3, z: -8},
            radius:   0.15,
            color:    {r: 233, g: 233, b: 233},
        },
        {
            position:  {x: 8, y: 3, z: -12},
            radius:    0.1,
            color:     {r: 255, g: 255, b: 255},
        },
        {
            position: {x: -5, y: -3, z: -8},
            radius:   0.1,
            color:    {r: 255, g: 255, b: 255},
        },
    ],

    Objects: [
        {
            type:     'ellipsoid',
            position: {x: -4, y: 0, z: -4},
            radius:   {x: 0.66, y: 4, z: 3},
            material: {
                color:        {r: 0, g: 255, b: 255},
                diffuse:      0.2,
                reflection:   0.8,
                specular:     20,
                specular_exp: 50,
            },
        },
        {
            type:     'sphere',
            position: {x: 0, y: 0, z: -4},
            radius:   0.75,
            material: {
                color:        {r: 255, g: 0, b: 255},
                diffuse:      0.2,
                reflection:   0.5,
                specular:     20,
                specular_exp: 50,
            },
        },
        {
            type:     'sphere',
            position: {x: -2, y: 0, z: -6},
            radius:   0.5,
            material: {
                color:        {r: 255, g: 255, b: 0},
                diffuse:      0.2,
                specular:     0.2,
                specular_exp: 5,
            },
        },
        {
            type: 'triangle',
            v1:   {x:  6, y: -5, z: -4},
            v2:   {x:  6, y: -5, z:  4},
            v3:   {x:  2, y:  0, z:  0},
            material: {
                color:        {r: 0, g: 0, b: 255},
                checkerboard: {r: 0, g: 0, b: 0},
                ambient:      1,
            },
        },
        {
            type:     'plane',
            normal:   {x: 0, y: 1, z: 0},
            distance: -10,
            material: {
                color:        {r: 255, g: 0, b: 0},
                checkerboard: {r: 0, g: 0, b: 0},
                diffuse:      1,
                ambient:      0,
            },
        },
        {
            type:  'box',
            far:  {x: -1, y: -1, z: -1},
            near: {x:  1, y:  1, z:  1},
            material: {
                color:        {r: 224, g: 223, b: 222},
                diffuse:      0.5,
                reflection:   0.5,
                specular:     20,
                specular_exp: 50,
            },
        },
    ],
};
