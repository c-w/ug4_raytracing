var SoftshadowsDemo = {
    GlobalAmbient: 0.15,
    BackgroundColor: {r: 0, g: 0, b: 0},

    Camera: {
        position:  {x: 20, y: 20, z: 20},
        direction: {x: -1, y: -1, z: -1},
        fov:       168,
    },

    Lights: [
        {
            position:  {x: 8, y: 5, z: 5},
            radius:    0.1,
            color:     {r: 255, g: 255, b: 255},
            is_finite: true,
        },
        {
            position:  {x: 5, y: 8, z: 5},
            radius:    0.2,
            color:     {r: 255, g: 255, b: 255},
            is_finite: true,
        },
    ],

    Objects: [
        {
            type:     'sphere',
            position: {x: 0, y: 0, z: 1},
            radius:   1,
            material: {
                color:        {r: 255, g: 0, b: 0},
                diffuse:      0.2,
                specular:     0.2,
                specular_exp: 5,
            },
        },
        {
            type:     'sphere',
            position: {x: 0, y: 1, z: 0},
            radius:   1,
            material: {
                color:        {r: 0, g: 255, b: 0},
                diffuse:      0.2,
                specular:     0.2,
                specular_exp: 5,
            },
        },
        {
            type:     'sphere',
            position: {x: 1, y: 0, z: 0},
            radius:   1,
            material: {
                color:        {r: 0, g: 0, b: 255},
                diffuse:      0.2,
                specular:     0.2,
                specular_exp: 5,
            },
        },
        {
            type:     'plane',
            normal:   {x: 0, y: 1, z: 0},
            distance: -10,
            material: {
                color:        {r: 123, g: 123, b: 123},
                diffuse:      1,
                ambient:      0,
            },
        },
    ],
};
