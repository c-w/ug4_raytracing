var FLT_EPS = 0.000001;

// set-up a canvas to render to
var canvas = new Canvas(Config.CanvasId, Config.CanvasWidth, Config.CanvasHeight);

function main(scene_cfg) {
    var renderer = new Renderer(canvas, new Scene(scene_cfg));

    // remember document title to be able to over-write it with progress bar
    var doc_title = document.title;

    // render the scene
    for (var y = 0; y < renderer.canvas.height; y++) {
        // update progress indicator in document title
        if (Config.ShowProgressInTitle) {
            var progress = 100 * y / renderer.canvas.height;
            document.title = 'Rendering... ' + progress.toFixed(2) + '%';
        }
        // render pixel
        for (var x = 0; x < renderer.canvas.width; x++) {
            var c = renderer.render_pixel(x, y);
            renderer.canvas.set_pixel(x, y, c);
        }
    }
    // display the scene
    renderer.canvas.draw();

    // restore original document title
    if (Config.ShowProgressInTitle) {
        document.title = doc_title;
    }
};
