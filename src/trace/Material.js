/**
 * Defines the material properties of some object: color, diffuse reflectance,
 * specular reflectance, reflectivity, refractiveness, etc.
 */
function Material(cfg) {
    this.color = new Color(cfg.color).mult(1 / 255);
    this.diffuse = cfg.diffuse === undefined ? 0 : cfg.diffuse;
    this.reflection = cfg.reflection === undefined ? 0 : cfg.reflection;
    this.specular = cfg.specular === undefined ? 0 : cfg.specular;
    this.specular_exp = cfg.specular_exp === undefined ? 0 : cfg.specular_exp;
    this.ambient = cfg.ambient === undefined ? 0 : cfg.ambient;
    // optional
    if (cfg.checkerboard) {
        this.checkerboard = new Color(cfg.checkerboard).mult(1 / 255);
    }
    if (cfg.refraction && cfg.refraction_index) {
        this.refraction = cfg.refraction;
        this.refraction_index = cfg.refraction_index;
    }
};

/**
 * Returns the color of the paterial at a specific point.
 * Using this instead of directly accessing the color attribute allows for
 * procedural textures etc to be generated
 */
Material.prototype.get_color = function(p) {
    if (this.checkerboard) {
        return (((Math.floor(p.x) + Math.floor(p.z)) % 2)
                ? this.checkerboard
                : this.color);
    }
    return this.color;
};
