/**
 * Tests a ray for intersection with all objects in the scene.
 * Returns whether or not the ray intersects any object in the scene and sets
 * the following ray properties:
 * - which object did the ray intersect
 * - time of intersection
 * - point of intersection
 */
function check_intersection(scene, ray) {
    var retval = Intersection.MISS;
    var closest = ray.hit_dist;
    for (var i = 0; i < scene.objects.length; i++) {
        var intersection = scene.objects[i].intersect(ray);
        if (intersection && intersection.dist < closest) {
            ray.hit_obj = intersection.obj;
            ray.hit_dist = intersection.dist;
            closest = intersection.dist;
            retval = intersection.type;
            if (intersection.hit_point) {
                ray.hit_point = intersection.hit_point;
            }
        }
    }
    if (ray.hit_obj && !ray.hit_point) {
        var dt = ray.direction.mult(ray.hit_dist);
        ray.hit_point = ray.origin.add(dt);
    }
    return retval;
};

/**
 * Computes the amount of light reaching some point by casting rays from the
 * point to the light source and checking if these rays are blocked.
 *
 * Supports finite/area lights (soft shadows) and point lights (hard shadows).
 * A primitive that implements `center_point()` can be a point light.
 * A primitive that also implements `random_point()` can be an area light.
 */
function calculate_shade(scene, point, light) {
    // find light direction and distance
    var light_dir = light.center_point().sub(point);
    var light_dist = light_dir.magnitude;
    light_dir = light_dir.normalize();
    // setup shadow rays
    var shade_pt = point.add(light_dir.mult(Config.HitpointOffset));
    var shadow_rays = [];
    if (light.is_finite) {
        // shoot rays to random points on light source
        for (var i = 0; i < Config.FiniteLightsourceSamples; i++) {
            var light_pos = light.random_point();
            var shadow_ray = new Ray(shade_pt, light_pos.sub(shade_pt));
            shadow_rays.push(shadow_ray);
        }
    } else {
        // shoot one ray to center of light source
        var shadow_ray = new Ray(shade_pt, light_dir);
        shadow_rays.push(shadow_ray);
    }
    // cast the shadow rays and infer the amount of light reaching the object
    var shade = shadow_rays.length;
    for (var i = 0; i < shadow_rays.length; i++) {
        var shadow_ray = shadow_rays[i];
        shadow_ray.hit_dist = light_dist;
        check_intersection(scene, shadow_ray);
        if (shadow_ray.hit_obj && !shadow_ray.hit_obj.is_light) {
            shade--;  // light fully blocked
        }
    }
    shade /= shadow_rays.length;
    return [shade, light_dir];
};

/**
 * The recursive ray-tracing function. Accumulates color by casting a primary
 * ray into the scene and splitting the ray into shadow/reflection/refraction
 * rays depending on the objects the initial ray intersects.
 */
function trace(scene, ray, trace_depth) {
    var intersection_result = check_intersection(scene, ray);
    if (ray.hit_dist >= Config.MaxRayDepth || ray.hit_obj === null) {
        return scene.bg_color;  // no intersection
    }
    var color = ray.hit_obj.material.get_color(ray.hit_point);

    // stop tracing if a light is hit
    if (ray.hit_obj.is_light) { return color; }

    // got an intersection: set color to ambient
    var rgb = color.mult(ray.hit_obj.material.ambient);

    // setup for phong shading calculations
    var N = ray.hit_obj.normal_at_point(ray.hit_point);
    var V = ray.direction;

    // add up contributions of each light
    for (var i = 0; i < scene.lights.length; i++) {
        var light = scene.lights[i];
        var shadow_info = calculate_shade(scene, ray.hit_point, light);
        var shade = shadow_info[0];
        var L = shadow_info[1];
        if (shade < FLT_EPS) { continue; }  // point if fully shaded

        var N_dot_L = N.dot(L);
        // diffuse contribution
        if (ray.hit_obj.material.diffuse) {
            if (N_dot_L < FLT_EPS) { continue; }
            var diff = shade * N_dot_L * ray.hit_obj.material.diffuse;
            rgb = rgb.add(color.mult(diff));
        }

        // specular contribution
        if (ray.hit_obj.material.specular) {
            var R = L.sub(N.mult(2 * N_dot_L));
            var V_dot_R = V.dot(R);
            if (V_dot_R < FLT_EPS) { continue; }
            var spec = (shade * ray.hit_obj.material.specular
                        * Math.pow(V_dot_R,
                                   ray.hit_obj.material.specular_exp));
            rgb = rgb.add(light.material.color.mult(spec));
        }
    }

    // reflection contribution
    var refl = ray.hit_obj.material.reflection;
    if (trace_depth < Config.MaxRecursionDepth && refl) {
        var R = V.sub(N.mult(2 * V.dot(N)));
        var refl_ray = new Ray(
            ray.hit_point.add(R.mult(Config.HitpointOffset)), R);
        var refl_color = trace(scene, refl_ray, trace_depth + 1);
        rgb = rgb.add(refl_color.mult(refl));
    }

    // refraction contribution
    var refr = ray.hit_obj.material.refraction;
    if (trace_depth < Config.MaxRecursionDepth && refr) {
        var incident = ray.direction;
        var normal = N;
        var n1, n2;
        var cosI = incident.dot(normal);
        if (cosI > 0) {
            // ray is inside of material
            n1 = ray.hit_obj.material.refraction_index;
            n2 = Config.AirRefractionIndex;
            normal = normal.invert();
        } else {
            // ray is outside of material
            n1 = Config.AirRefractionIndex;
            n2 = ray.hit_obj.material.refraction_index;
            cosI = -cosI;
        }
        var n = n1 / n2;
        var cosT = 1 - n * n * (1 - cosI * cosI);
        var refr_ray;
        if (cosT < 0) {
            // total internal reflection
            var R = V.sub(normal.mult(2 * V.dot(normal)));
            refr_ray = new Ray(
                ray.hit_point.add(normal.mult(Config.HitpointOffset)), R);
        } else {
            // normal refraction
            cosT = Math.sqrt(cosT);
            var T = incident.mult(n).add(normal.mult(n * cosI - cosT));
            refr_ray = new Ray(
                ray.hit_point.sub(normal.mult(Config.HitpointOffset)), T);
        }
        var refr_color = trace(scene, refr_ray, trace_depth + 1);
        rgb = rgb.add(refr_color.mult(refr));
    }

    return rgb.clip(1);
};
