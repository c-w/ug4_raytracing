/**
 * Reads a scene description file and converts it to the representation used by
 * the raytracer.
 */
function Scene(scene_cfg) {
    this.lights = []; this.objects = [];
    this.read_bgcolor_from_config(scene_cfg);
    this.read_camera_from_config(scene_cfg);
    this.read_lights_from_config(scene_cfg);
    this.read_objects_from_config(scene_cfg);
};

Scene.prototype.read_bgcolor_from_config = function(scene_cfg) {
    this.bg_color = new Color(scene_cfg.BackgroundColor).mult(1 / 255);
};

Scene.prototype.read_camera_from_config = function(scene_cfg) {
    this.camera = new Camera(new Vector(scene_cfg.Camera.position),
                             new Vector(scene_cfg.Camera.direction),
                             scene_cfg.Camera.fov);
};

Scene.prototype.read_lights_from_config = function(scene_cfg) {
    for (var i = 0; i < scene_cfg.Lights.length; i++) {
        var cfg = scene_cfg.Lights[i];
        var light = new Light(cfg);
        this.add_light(light);
    }
};

Scene.prototype.read_objects_from_config = function(scene_cfg) {
    for (var i = 0; i < scene_cfg.Objects.length; i++) {
        var cfg = scene_cfg.Objects[i];
        if (!cfg.material.ambient) {
            cfg.material.ambient = scene_cfg.GlobalAmbient;
        }
        var material = new Material(cfg.material);
        var obj;
        switch (cfg.type.toLowerCase()) {
            case 'ellipsoid':
                obj = new Ellipsoid(new Vector(cfg.position),
                                    new Vector(cfg.radius));
                break;
            case 'sphere':
                obj = new Sphere(new Vector(cfg.position), cfg.radius);
                break;
            case 'triangle':
                obj = new Triangle(new Vector(cfg.v1),
                                   new Vector(cfg.v2),
                                   new Vector(cfg.v3));
                break;
            case 'plane':
                obj = new Plane(new Vector(cfg.normal), cfg.distance);
                break;
            case 'box':
                obj = new Box(new Vector(cfg.far), new Vector(cfg.near));
                break;
            default:
                console.log('skipping unsupported object type: ' + cfg.type);
                break;
        }
        obj.material = material;
        this.add_object(obj);
    }
};

Scene.prototype.add_light = function(light) {
    if (Config.ShowLightsources) {
        // also push the lights into objects so that we can visualize them
        this.objects.push(light);
    }
    this.lights.push(light);
};

Scene.prototype.add_object = function(obj) {
    this.objects.push(obj);
};

Scene.random = function() {
    var random_scene = {
        BackgroundColor: {r: 0, g: 0, b: 0},
        GlobalAmbient: 0.15,
        Camera: {
            position:  {x: 20, y: 0, z: -50},
            direction: {x: 0, y: 0, z: 1},
            fov:       168,
        },
        Lights: [
            {
                position: {x: 2, y: 0, z: 0},
            },
            {
                position: {x: 0, y: 10, z: 7.5},
            },
        ],
        Objects: [
            {
                type:     'plane',
                normal:   {x: 0, y: 1, z: 0},
                distance: -10,
                material: {
                    color:    {r: Math.round(Math.random() * 255),
                               g: Math.round(Math.random() * 255),
                               b: Math.round(Math.random() * 255)},
                    diffuse:  0.5,
                },
            },
        ],
    };
    for (var i = 0; i < Config.RandomSceneNumObjects;) {
        var r = Math.round(Math.random() * 255);
        var g = Math.round(Math.random() * 255);
        var b = Math.round(Math.random() * 255);
        var x = (Math.random() * Config.RandomSceneObjectSpreadX
                 + Config.RandomSceneObjectOffsetX);
        var y = (Math.random() * Config.RandomSceneObjectSpreadY
                 + Config.RandomSceneObjectOffsetY);
        var z = (Math.random() * Config.RandomSceneObjectSpreadZ
                 + Config.RandomSceneObjectOffsetZ);
        var size = Math.random();
        if (Math.random() < Config.RandomSceneSphereProbability) {
            // add a random sphere
            random_scene.Objects.push(
                {
                    type: 'sphere',
                    position: {x: x, y: y, z: z},
                    radius: size,
                    material: {
                        color: {r: r, g: g, b: b},
                        diffuse: 0.2,
                        reflection: 0.8,
                        specular: 2,
                        specular_exp: 50,
                    },
                }
            );
            i++;
            continue;
        }
        if (Math.random() < Config.RandomSceneBoxProbability) {
            // add a random box
            random_scene.Objects.push(
                {
                    type: 'box',
                    far: {x: x, y: y, z: z},
                    near: {x: x + size, y: y + size, z: z + size},
                    material: {
                        color: {r: r, g: g, b: b},
                        diffuse: 0.8,
                        reflection: 0.2,
                        specular: 0.5,
                        specular_exp: 5,
                    },
                }
            );
            i++;
            continue;
        }
    }
    return random_scene;
};

///////////////////////////
// virtual scene objects //
///////////////////////////

function Light(cfg) {
    var sphere = new Sphere(new Vector(cfg.position), cfg.radius || 0.1,
                            new Material({color: cfg.color
                                                 || {r: 255, g: 255, b: 255}}));
    if (cfg.is_finite) { sphere.is_finite = cfg.is_finite; }
    sphere.is_light = true;
    return sphere;
};

function Ray(origin, direction) {
    this.origin = origin;
    this.direction = direction.normalize();
    this.hit_dist = Config.MaxRayDepth;
    this.hit_obj = null;
    this.hit_point = null;
};

function Camera(position, direction, fov) {
    this.position = position;
    this.direction = direction;
    this.field_of_view = fov;
};

function Intersection(type, obj, dist, hit_point) {
    this.type = type;
    this.obj = obj || null;
    this.dist = dist || Config.MaxRayDepth;
    this.point = hit_point || null;
};
Intersection.HIT = 1;
Intersection.MISS = 0;
Intersection.INSIDE = -1;
