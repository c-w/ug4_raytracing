/**
 * Primitives that the ray-tracer can render.
 *
 * Every primitive has to implement the following functions:
 * - normal_at_point(p): return the normal vector on the shape at point p
 * - intersect(ray):     return whether or not the ray intersects the shape
 * - bounding_box():     return a cube containing the entire primitive
 */

/////////////////////////
// ellipsoid primitive //
/////////////////////////

function Ellipsoid(position, radius, material) {
    this.position = position;
    this.radius = radius;
    this.sq_radius = radius.mult(radius);
    this.material = material;
};

Ellipsoid.prototype.bounding_box = function() {
    if (!this._bbox) {
        this._bbox = new Box(this.position.sub(this.radius),
                             this.position.add(this.radius));
    }
    return this._bbox;
};

Ellipsoid.prototype.normal_at_point = function(p) {
    if (!this._M) {
        this._M = new Matrix([[2 / this.sq_radius.x, 0, 0],
                              [0, 2 / this.sq_radius.y, 0],
                              [0, 0, 2 / this.sq_radius.z]]);
    }
    return this._M.mult(p.sub(this.position)).normalize();
};

Ellipsoid.prototype.intersect = function(ray) {
    var oc = ray.origin.sub(this.position);
    var dir = ray.direction.normalize();
    var a = (  (dir.x * dir.x) / this.sq_radius.x
             + (dir.y * dir.y) / this.sq_radius.y
             + (dir.z * dir.z) / this.sq_radius.z);
    var b = (  (2 * oc.x * dir.x) / this.sq_radius.x
             + (2 * oc.y * dir.y) / this.sq_radius.y
             + (2 * oc.z * dir.z) / this.sq_radius.z);
    var c = (  (oc.x * oc.x) / this.sq_radius.x
             + (oc.y * oc.y) / this.sq_radius.y
             + (oc.z * oc.z) / this.sq_radius.z
             - 1);
    var det = ((b * b) - (4 * a * c));
    if (det < FLT_EPS) {
        return null;
    }
    if ((-FLT_EPS < a && a < FLT_EPS) || (-FLT_EPS < b && b < FLT_EPS)) {
        return null;
    }
    det = Math.sqrt(det);
    var i1 = (-b - det) / (2 * a);
    var i2 = (-b + det) / (2 * a);
    if (i2 > -FLT_EPS) {
        if (i1 < FLT_EPS) {
            return new Intersection(Intersection.INSIDE, this, i2);
        } else {
            return new Intersection(Intersection.HIT, this, i1);
        }
    }
    return null;
};

////////////////////////
// triangle primitive //
////////////////////////

function Triangle(v1, v2, v3, material) {
    this.v1 = v1;
    this.v2 = v2;
    this.v3 = v3;
    this.material = material;
};

Triangle.prototype.bounding_box = function() {
    if (!this._bbox) {
        var minx = Math.min(this.v1.x, this.v2.x, this.v3.x);
        var maxx = Math.max(this.v1.x, this.v2.x, this.v3.x);
        var miny = Math.min(this.v1.y, this.v2.y, this.v3.y);
        var maxy = Math.max(this.v1.y, this.v2.y, this.v3.y);
        var minz = Math.min(this.v1.z, this.v2.z, this.v3.z);
        var maxz = Math.max(this.v1.z, this.v2.z, this.v3.z);
        this._bbox = new Box(new Vector(minx, miny, minz),
                             new Vector(maxx, maxy, maxz));
    }
    return this._bbox;
};

Triangle.prototype.normal_at_point = function(p) {
    if (!this.normal) {
        var e1 = this.v2.sub(this.v1);
        var e2 = this.v3.sub(this.v1);
        this.normal = e1.cross(e2).normalize();
    }
    return this.normal;
};

Triangle.prototype.intersect = function(ray) {
    var p1 = this.v1;
    var p2 = this.v2;
    var p3 = this.v3;
    var e1 = p2.sub(p1);
    var e2 = p3.sub(p1);
    var s1 = ray.direction.cross(e2);
    var divisor = s1.dot(e1);
    if (-FLT_EPS < divisor && divisor < FLT_EPS) {
        return new Intersection(Intersection.MISS);
    }
    var inv_divisor = 1 / divisor;
    var d = ray.origin.sub(p1);
    var b1 = d.dot(s1) * inv_divisor;
    if (b1 < FLT_EPS || b1 > 1 - FLT_EPS) {
        return new Intersection(Intersection.MISS);
    }
    var s2 = d.cross(e1);
    var b2 = ray.direction.dot(s2) * inv_divisor;
    if (b2 < FLT_EPS || b1 + b2 > 1 - FLT_EPS) {
        return new Intersection(Intersection.MISS);
    }
    var t = e2.dot(s2) * inv_divisor;
    return new Intersection(Intersection.HIT, this, t);
};

///////////////////
// box primitive //
///////////////////

function Box(min, max, material) {
    this.min = min;
    this.max = max;
    this.material = material;
};

Box.prototype.bounding_box = function() {
    if (!this._bbox) {
        this._bbox = new Box(this.min, this.max);
    }
    return this._bbox;
}

Box.prototype.intersect = function(ray) {
    var tmin, tmax, tymin, tymax, tzmin, tzmax;
    if (ray.direction.x > -FLT_EPS) {
        tmin = (this.min.x - ray.origin.x) / ray.direction.x;
        tmax = (this.max.x - ray.origin.x) / ray.direction.x;
    }
    else {
        tmin = (this.max.x - ray.origin.x) / ray.direction.x;
        tmax = (this.min.x - ray.origin.x) / ray.direction.x;
    }
    if (ray.direction.y > -FLT_EPS) {
        tymin = (this.min.y - ray.origin.y) / ray.direction.y;
        tymax = (this.max.y - ray.origin.y) / ray.direction.y;
    } else {
        tymin = (this.max.y - ray.origin.y) / ray.direction.y;
        tymax = (this.min.y - ray.origin.y) / ray.direction.y;
    }
    if ((tmin > tymax) || (tymin > tmax)) {
        return null;
    }
    if (tymin > tmin) {
        tmin = tymin;
    }
    if (tymax < tmax) {
        tmax = tymax;
    }
    if (ray.direction.z > -FLT_EPS) {
        tzmin = (this.min.z - ray.origin.z) / ray.direction.z;
        tzmax = (this.max.z - ray.origin.z) / ray.direction.z;
    } else {
        tzmin = (this.max.z - ray.origin.z) / ray.direction.z;
        tzmax = (this.min.z - ray.origin.z) / ray.direction.z;
    }
    if ((tmin > tzmax) || (tzmin > tmax)) {
        return null;
    }
    if (tzmin > tmin) {
        tmin = tzmin;
    }
    if (tzmax < tmax) {
        tmax = tzmax;
    }
    if (tmin > tmax || tmin < FLT_EPS) {
        return null;
    }
    return new Intersection(Intersection.HIT, this, tmin);
};

Box.prototype.normal_at_point = function(p) {
    if (Math.abs(p.x - this.min.x) < FLT_EPS) {
        return new Vector(1, 0, 0);
    }
    if (Math.abs(p.x - this.max.x) < FLT_EPS) {
        return new Vector(-1, 0, 0);
    }
    if (Math.abs(p.y - this.min.y) < FLT_EPS) {
        return new Vector(0, 1, 0);
    }
    if (Math.abs(p.y - this.max.y) < FLT_EPS) {
        return new Vector(0, -1, 0);
    }
    if (Math.abs(p.z - this.min.z) < FLT_EPS) {
        return new Vector(0, 0, 1);
    }
    if (Math.abs(p.z - this.max.z) < FLT_EPS) {
        return new Vector(0, 0, -1);
    }
};

//////////////////////
// sphere primitive //
//////////////////////

function Sphere(position, radius, material) {
    this.position = position;
    this.radius = radius;
    this.material = material;
    this.sq_radius = radius * radius;
};

Sphere.prototype.bounding_box = function() {
    if (!this._bbox) {
        this._bbox = new Box(this.position.sub(this.radius),
                             this.position.add(this.radius));
    }
    return this._bbox;
};

Sphere.prototype.intersect = function(ray) {
    var v = ray.origin.sub(this.position);
    var b = -v.dot(ray.direction);
    var det = b * b - v.dot(v) + this.sq_radius;
    if (det > -FLT_EPS) {
        det = Math.sqrt(det);
        var i1 = b - det;
        var i2 = b + det;
        if (i2 > -FLT_EPS) {
            if (i1 < FLT_EPS) {
                return new Intersection(Intersection.INSIDE, this, i2);
            } else {
                return new Intersection(Intersection.HIT, this, i1);
            }
        }
    }
    return null;
};

Sphere.prototype.normal_at_point = function(p) {
    return p.sub(this.position).normalize();
};

/**
 * Sample a random point from the sphere.
 * Used for area light/soft shadow generation
 */
Sphere.prototype.random_point = function() {
    var theta = 2 * Math.PI * Math.random();
    var phi = Math.acos(2 * Math.random() - 1);
    var noise = new Vector(this.radius * Math.sin(phi) * Math.cos(theta),
                           this.radius * Math.sin(phi) * Math.sin(theta),
                           this.radius * Math.cos(phi));
    return this.position.add(noise);
};

/**
 * Returns the central point of the sphere.
 * Used for point light generation
 */
Sphere.prototype.center_point = function() {
    return this.position;
};

/////////////////////
// plane primitive //
/////////////////////

function Plane(normal, d, material) {
    this.normal = normal;
    this.d = d;
    this.material = material;
};

Plane.prototype.bounding_box = function() {
    if (!this._bbox) {
        this._bbox = new Box(Vector.INF.invert(), Vector.INF);
    }
    return this._bbox;
};

Plane.prototype.intersect = function(ray) {
    var v = this.normal.dot(ray.direction);
    if (-FLT_EPS < v && v < FLT_EPS) {
        return null;  // ray is parallel to plane
    }

    var hit_dist = (this.d - this.normal.dot(ray.origin)) / v;
    if (hit_dist < FLT_EPS) {
        return null;  // ray is behind plane
    }
    return new Intersection(Intersection.HIT, this, hit_dist);
};

Plane.prototype.normal_at_point = function(p) {
    return this.normal;
};
