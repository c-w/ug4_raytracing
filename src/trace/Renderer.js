/**
 * The Renderer class converts between screen and scene space.
 */
function Renderer(canvas, scene) {
    this.canvas = canvas;
    this.scene = scene;
    this.init_views();
};

/**
 * Setup variables for screen<->camera<->scene conversions.
 */
Renderer.prototype.init_views = function() {
    var camera = this.scene.camera;
    var world_width = this.canvas.width;
    var world_height = this.canvas.height;
    var world_up = new Vector(0, -1, 0);
    // compute view limits
    this.eye_vector = camera.direction.sub(camera.position);
    this.viewpoint_right = this.eye_vector.cross(world_up).normalize();
    this.viewpoint_up = this.viewpoint_right.cross(this.eye_vector).normalize();
    // camera <-> screen space conversion
    this.half_width = Math.tan(Math.PI * (camera.field_of_view / 2) / 180);
    this.half_height = (world_height / world_width) * this.half_width;
    this.pixel_width = (this.half_width * 2) / (world_width - 1);
    this.pixel_height = (this.half_height * 2) / (world_height - 1);
};

/**
 * Render the pixel at (x, y) by recursive ray tracing.
 */
Renderer.prototype.render_pixel = function(x, y) {
    // compute direction of pixel from eye position
    var X = this.viewpoint_right.mult((x * this.pixel_width) - this.half_width);
    var Y = this.viewpoint_up.mult((y * this.pixel_height) - this.half_height);
    // trace primary ray
    var ray = new Ray(this.scene.camera.position,
                      this.eye_vector.add(X).add(Y).normalize());
    var color = trace(this.scene, ray, 0);
    return color;
};
