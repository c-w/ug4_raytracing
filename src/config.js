var Config = {
    // ray-tracing constants
    MaxRecursionDepth: 3,
    MaxRayDepth: 1000,
    HitpointOffset: 0.0001,
    FiniteLightsourceSamples: 100,
    AirRefractionIndex: 1.0,
    ShowLightsources: true,

    // canvas constants
    CanvasWidth: 640,
    CanvasHeight: 480,
    CanvasId: 'canvas',
    ShowProgressInTitle: true,

    // constants for random scene generation
    RandomSceneNumObjects: 80,
    RandomSceneObjectSpreadX: 10,
    RandomSceneObjectSpreadY: 10,
    RandomSceneObjectSpreadZ: 10,
    RandomSceneObjectOffsetX: -5,
    RandomSceneObjectOffsetY: -5,
    RandomSceneObjectOffsetZ: 0,
    RandomSceneSphereProbability: 0.8,
    RandomSceneBoxProbability: 0.2,
};
